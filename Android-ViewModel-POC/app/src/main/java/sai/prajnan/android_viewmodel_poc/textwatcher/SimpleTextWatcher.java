package sai.prajnan.android_viewmodel_poc.textwatcher;

import android.text.Editable;
import android.text.TextWatcher;

/**
 * Created by prajnan on 9/25/15.
 */
public abstract class SimpleTextWatcher implements TextWatcher {
    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {}

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {}

    @Override
    public void afterTextChanged(Editable editable) {
        onTextChanged(editable.toString());
    }

    public abstract void onTextChanged(String newValue);
}
