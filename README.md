## What's this project about: ##
This is proof of concept to play around with the new Android Data Binding library.

## Environment Setup: ##

Add these to the top level gradle file:

dependencies {
       classpath "com.android.tools.build:gradle:1.3.0"
       classpath "com.android.databinding:dataBinder:1.0-rc1"
   }

To each module where we need data binding, add these to their respective gradle files:
apply plugin: 'com.android.application'
apply plugin: 'com.android.databinding'

--------------------------------------------------------------------

## ViewModels ##
The ViewModels in android have to extend the BaseObservable class.

Currently, the library supports only one-way data binding, so implemented a custom two-way data binding. 
