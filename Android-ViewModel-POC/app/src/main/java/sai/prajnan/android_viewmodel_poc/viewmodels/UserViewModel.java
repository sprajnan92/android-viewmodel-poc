package sai.prajnan.android_viewmodel_poc.viewmodels;

import android.content.Context;
import android.databinding.BaseObservable;
import android.databinding.Bindable;
import android.support.design.widget.Snackbar;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Toast;

import sai.prajnan.android_viewmodel_poc.BR;
import sai.prajnan.android_viewmodel_poc.commands.ButtonCommand;
import sai.prajnan.android_viewmodel_poc.textwatcher.SimpleTextWatcher;

/**
 * Created by prajnan on 9/25/15.
 */
public class UserViewModel extends BaseObservable {

    private String username;
    private String email;
    private Context context;

    public UserViewModel(final Context context){
        this.context = context;
    }

    @Bindable
    public String getUsername() {
        return this.username;
    }

    @Bindable
    public String getEmail() {
        return this.email;
    }

    public void setUsername(String username) {
        this.username = username;
        notifyPropertyChanged(BR.username);
    }

    public void setEmail(String email) {
        this.email = email;
        notifyPropertyChanged(BR.email);
    }

    public ButtonCommand getButtonCommand(){
        return new ButtonCommand() {
            @Override
            public void onCommandExecute() {
                Toast.makeText(context, "Command Handling Successful", Toast.LENGTH_LONG).show();
            }
        };
    }

    public TextWatcher getOnUsernameChanged() {
        return new SimpleTextWatcher() {
            @Override
            public void onTextChanged(String newValue) {
                setUsername(newValue);
            }
        };
    }

}
