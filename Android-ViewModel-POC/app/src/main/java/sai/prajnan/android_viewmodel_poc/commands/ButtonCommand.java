package sai.prajnan.android_viewmodel_poc.commands;

import android.view.View;
import android.view.View.OnClickListener;

/**
 * Created by prajnan on 9/25/15.
 */
public abstract class ButtonCommand implements OnClickListener {
    @Override
    public void onClick(View view) {
        onCommandExecute();
    }

    public abstract void onCommandExecute();
}
